Dropdown button with text

- on:click: event fired when button is clicked
- label: dropdown button label.

Example of parent component including dropdown button

```
<script>
  import DropdownButton from "@identomat-web-module-manage/dropdown-button";

  function handler(event) {
    // value returned is an object {open: true | false}
    // in event.detail
  }
</script>

<DropdownButton on:click={handler} label={'Identomat'} />
```