describe('component tests', () => {

    beforeEach(() => {
        cy.visit('/')})

    it('slot context exists after clicking on parent element', () => {
        cy.get('.context-parent').children().should('not.exist')
        cy.get('[style="display: table;"]').click()
        .get('.context-parent').should('exist')
    })

    it('tooltip contains text', () => {
        cy.get('.details-tip').contains('Details')
    })

    it('context menu is visible after clicking button', () => {
        cy.get('[style="display: table;"]').click()
        .get('.context-parent').find('div').eq(0).should('exist')
    })

    it('menu is still visible after clicking itself/items', () => {
        cy.get('.context-parent').click({force: true})
        cy.get('.context-parent').children().should('exist')
    })

    it('checks if tooltips is visible on hover', () => {
        cy.get('.options').trigger('mouseover')
        .get('.details-tip').should('exist')
    })


}) 